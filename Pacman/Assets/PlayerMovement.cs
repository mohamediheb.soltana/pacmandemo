using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public int coins;
    public float forceSpeed = 1000f;
    public Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("z"))
        {
            rb.AddForce(forceSpeed, 0, 0);
        }
        if (Input.GetKey("s"))
        {
            rb.AddForce(-forceSpeed, 0, 0);
        }
        if (Input.GetKey("q"))
        {
            rb.AddForce(0, 0, forceSpeed);
        }
        if (Input.GetKey("d"))
        {
            rb.AddForce(0, 0, -forceSpeed);
        }
    }
    
    public void OnTriggerEnter(Collider Col)
    {
        if (Col.gameObject.tag == "Coin")
        {
            coins = coins + 1;
            Col.gameObject.SetActive(false);
        }

    }
}
